import time
import RPi.GPIO as GPIO

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

#Set up connection to firebase
cred = credentials.Certificate('/home/pi/vendpkit-firebase-adminsdk-byovv-85d1fcd534.json')
firebase_admin.initialize_app(cred)

#Get a reference to the makeDrink document
doc_ref = firestore.client().collection(u'rpi').document(u'makeDrink')

#Set up GPIO pins
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT, initial=1) #Credit pin
GPIO.setup(8, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) #inhibiting pin

#Pins relating to vending machine buttons
GPIO.setup(16, GPIO.OUT, initial=1) #Button 12
GPIO.setup(18, GPIO.OUT, initial=1) #Button 11
GPIO.setup(22, GPIO.OUT, initial=1) #Button 10
GPIO.setup(24, GPIO.OUT, initial=1) #Button 9
GPIO.setup(19, GPIO.OUT, initial=1) #Button 8
GPIO.setup(21, GPIO.OUT, initial=1) #Button 7
GPIO.setup(23, GPIO.OUT, initial=1) #Button 6
GPIO.setup(29, GPIO.OUT, initial=1) #Button 5
GPIO.setup(31, GPIO.OUT, initial=1) #Button 4
GPIO.setup(33, GPIO.OUT, initial=1) #Button 3
GPIO.setup(35, GPIO.OUT, initial=1) #Button 2
GPIO.setup(37, GPIO.OUT, initial=1) #Button 1

#Credit the machine with the correct amount
def creditMachine(price):
    for i in range(price):
        GPIO.output(7, 0)
        time.sleep(.060)
        GPIO.output(7, 1)
        time.sleep(.040)

#Press the right button
def press(button):
    if(button == "Espresso"):
        GPIO.output(37, 0)
        time.sleep(1)
        GPIO.output(37, 1)
        
    elif(button == "Americano"):
        GPIO.output(35, 0)
        time.sleep(1)
        GPIO.output(35, 1)

    elif(button == "Cappuccino"):
        GPIO.output(31, 0)
        time.sleep(1)
        GPIO.output(31, 1)    
        
    elif(button == "Spiced Cappuccino - Milky"):
        GPIO.output(33, 0)
        time.sleep(1)
        GPIO.output(33, 1)
        
    elif(button == "Mochaccino"):
        GPIO.output(29, 0)
        time.sleep(1)
        GPIO.output(29, 1)

    elif(button == "Hot Chocolate - Strong"):
        GPIO.output(23, 0)
        time.sleep(1)
        GPIO.output(23, 1)

    elif(button == "Spiced Cappuccino - Strong"):
        GPIO.output(21, 0)
        time.sleep(1)
        GPIO.output(21, 1)
        
    elif(button == "Hot Chocolate - Milky"):
        GPIO.output(19, 0)
        time.sleep(1)
        GPIO.output(19, 1)
        
    elif(button == "Chai Latte"):
        GPIO.output(24, 0)
        time.sleep(1)
        GPIO.output(24, 1)    

    elif(button == "Chai & Choc Latte"):
        GPIO.output(22, 0)
        time.sleep(1)
        GPIO.output(22, 1)
        
    elif(button == "Hot Water"):
        GPIO.output(18, 0)
        time.sleep(1)
        GPIO.output(18, 1)

    else:
        GPIO.output(16, 0)
        time.sleep(1)
        GPIO.output(16, 1)
        
#loop forever
try:
    while(True):
        try:
            time.sleep(.100)
            busy = doc_ref.get().get('busy')  
            if(busy):
                drinkName = doc_ref.get().get('drinkName')
                price = doc_ref.get().get('price')

                #Credit machine
                creditMachine(price)

                #Press button
                press(drinkName)

                #Wait for short time for vending machine to start making drink
                time.sleep(5)
                
                #Wait for drink to finish
                GPIO.wait_for_edge(8, GPIO.FALLING)
               
                #Re-enable app menu buttons
                try:
                    doc_ref.update({"busy": False, "drinkName": "", "price": 0})
                except:
                    print("Error!")
                    break
        
        except: 
            print('Error!')
            break

    

#loop back to start and look again

#clean up when done     
except KeyboardInterrupt:
    GPIO.cleanup()
    print("done")
