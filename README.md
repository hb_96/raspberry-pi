# Raspberry Pi

This repository is for all the code that was run on the Raspberry Pi.
busy.py was used to test if the Raspberry Pi can tell when the vending machine was busy dispensing a drink or not.
buttons.py was used to test if the Raspberry Pi could interface with all of the vending machine buttons.
credit.py was used to test if the Raspberry Pi could credit the vending machine
Lastly, the script that would run on the Raspberry Pi permanently is fire.py. It Uses all of the functionality that has been tested
using previous scripts.