import time
import RPi.GPIO as GPIO

#Set up GPIO pins
GPIO.setmode(GPIO.BOARD)

GPIO.setup(16, GPIO.OUT, initial=1)
GPIO.setup(18, GPIO.OUT, initial=1)
GPIO.setup(22, GPIO.OUT, initial=1)
GPIO.setup(24, GPIO.OUT, initial=1)
GPIO.setup(19, GPIO.OUT, initial=1)
GPIO.setup(21, GPIO.OUT, initial=1)
GPIO.setup(23, GPIO.OUT, initial=1)
GPIO.setup(29, GPIO.OUT, initial=1)
GPIO.setup(31, GPIO.OUT, initial=1)
GPIO.setup(33, GPIO.OUT, initial=1)
GPIO.setup(35, GPIO.OUT, initial=1)
GPIO.setup(37, GPIO.OUT, initial=1)

def press(button):
    if(button == 1):
        GPIO.output(37, 0)
        time.sleep(1)
        GPIO.output(37, 1)
        
    elif(button == 2):
        GPIO.output(35, 0)
        time.sleep(1)
        GPIO.output(35, 1)
        
    elif(button == 4):
        GPIO.output(33, 0)
        time.sleep(1)
        GPIO.output(33, 1)

    elif(button == 3):
        GPIO.output(31, 0)
        time.sleep(1)
        GPIO.output(31, 1)
        
    elif(button == 5):
        GPIO.output(29, 0)
        time.sleep(1)
        GPIO.output(29, 1)

    elif(button == 6):
        GPIO.output(23, 0)
        time.sleep(1)
        GPIO.output(23, 1)

    elif(button == 7):
        GPIO.output(21, 0)
        time.sleep(1)
        GPIO.output(21, 1)
        
    elif(button == 8):
        GPIO.output(19, 0)
        time.sleep(1)
        GPIO.output(19, 1)
        
    elif(button == 9):
        GPIO.output(24, 0)
        time.sleep(1)
        GPIO.output(24, 1)    

    elif(button == 10):
        GPIO.output(22, 0)
        time.sleep(1)
        GPIO.output(22, 1)
        
    elif(button == 11):
        GPIO.output(18, 0)
        time.sleep(1)
        GPIO.output(18, 1)

    else:
        GPIO.output(16, 0)
        time.sleep(1)
        GPIO.output(16, 1)
    
try:
    while(True):
        button = input("Press button: ")
        press(int(button))
    
except KeyboardInterrupt:
    print("cleaning up")
    GPIO.cleanup()
    