import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT, initial=1)
GPIO.setup(8, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)


def creditMachine(price):
    for i in range(price):
        GPIO.output(7, 0)
        time.sleep(.060)
        GPIO.output(7, 1)
        time.sleep(.040)

def busy(channel):
    if(GPIO.input(8)):
        print("busy")
    else:
        print("done")
    
GPIO.add_event_detect(8, GPIO.BOTH, callback=busy, bouncetime=10)

    
try:
    while(True):
        time.sleep(0)
        amount = input("How much?: ")
        creditMachine(int(amount))
except KeyboardInterrupt:
    print("done")
    GPIO.cleanup()
