import time
import RPi.GPIO as GPIO

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

#Set up connection to firebase
cred = credentials.Certificate('/home/pi/vendpkit-firebase-adminsdk-byovv-85d1fcd534.json')
firebase_admin.initialize_app(cred)

#Get a reference to the makeDrink document
doc_ref = firestore.client().collection(u'rpi').document(u'makeDrink')

GPIO.setmode(GPIO.BOARD)
GPIO.setup(8, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

def busy(channel):
    if(GPIO.input(8)):
        print("Busy making drink")
		try:
			doc_ref.update({"busy": True})
		except:
			print("Error!")
    else:
        print("Done with drink")
		try:
			doc_ref.update({"busy": False})
		except:
			print("Error!")
    
GPIO.add_event_detect(8, GPIO.BOTH, callback=busy, bouncetime=100)

    
try:
    while(True):
        time.sleep(0)
except KeyboardInterrupt:
    print("Clean up!")
    GPIO.cleanup()